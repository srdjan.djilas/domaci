-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_SRDJAN_termin_7
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('203','Ceska'),('226','Ekvatorijalna Gvineja'),('250','Francuska'),('36','Australija'),('4','Avganistan'),('591','Panama'),('688','Srbija'),('76','Brazil');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pttBroj` varchar(10) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `drzava_id` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_drzava_id` (`drzava_id`),
  CONSTRAINT `grad_ibfk_1` FOREIGN KEY (`drzava_id`) REFERENCES `drzava` (`kod`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'21000','Novi Sad','688'),(2,'11000','Beograd','688'),(3,'69002','Breclav','203'),(4,'10000','Prag','203'),(5,'0601','Panama City','591'),(6,'23906','Angra Dos Reis','76'),(7,'11600','Sao Sebastiao','76'),(8,'53250','Charchigne','250'),(9,'75000','Pariz','250'),(10,'3220','Geelong','36'),(11,'3000','Melburn','36');
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1,'Finansije'),(2,'Metalurgija'),(3,'Zdravstvo'),(4,'Obrazovanje'),(5,'Osiguranje'),(6,'Agrar'),(7,'Petrohemija'),(8,'Transport'),(9,'Auto Industrija'),(10,'IT'),(11,'Gradjevina');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `gradID` int(11) DEFAULT NULL,
  PRIMARY KEY (`prodavnicaID`),
  KEY `fk_gradID` (`gradID`),
  CONSTRAINT `prodavnica_ibfk_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (1,'NS prod 3 Veg',1),(2,'NS prod 1 Lev',1),(3,'Ns prod 2 Lev',1),(4,'Ns prod 1 Sym',1),(5,'Bg prod1 Nord',2),(6,'Bg prod 1 Delta',2),(7,'CZ prod 1 Skod',4),(8,'CZ prod 2 Skod',4),(9,'CZ prod 1 Agr',4),(10,'CZ prod 2 Agr',4),(11,'CZ prod 3 Gum',3),(12,'FRA prod 1 Lac',8),(13,'FRA prod 2 Lac',8),(14,'FRA prod 1 Bio',8),(15,'FRA prod 1 Axa',9),(16,'FRA prod2  Axa',9),(17,'FRA prod 1 BNP',9),(18,'FRA prod 2 BNP',9),(19,'BRA prod 1 Pet',7),(20,'BRA prod 2 Pet',7),(21,'BRA prod 1 Pet',6),(22,'AUT prod 1 CSL',11),(23,'AUT prod 2 CSL',11),(24,'AUT prod 1 BAR',10);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `proizvodId` int(11) NOT NULL,
  `prodavnicaID` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  KEY `fk_proizvodId` (`proizvodId`),
  KEY `fk_prodavnicaID` (`prodavnicaID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (34,1,20000.00),(34,1,20000.00),(32,5,1000000.00),(34,4,78230.00),(32,5,34543.00),(31,6,1170000.00),(25,7,3400000.00),(26,7,2890000.00),(25,8,3300000.00),(27,9,150.00),(28,9,170.00),(27,10,145.00),(28,10,210.00),(29,11,11250.00),(30,11,9999.00),(14,12,189.00),(15,12,123.00),(16,12,500.00),(16,13,450.00),(18,14,29000.00),(8,15,10000000.00),(10,15,3500.00),(11,17,1234511.00),(12,18,55555.00),(19,19,220.00),(21,19,289.00),(5,24,10.00);
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barkod` varchar(13) NOT NULL,
  `proizvodjacID` int(11) DEFAULT NULL,
  `kategorijaID` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodID`),
  KEY `fk_kategorijaID` (`kategorijaID`),
  KEY `fk_proizvodjacID` (`proizvodjacID`),
  CONSTRAINT `proizvod_ibfk_1` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`),
  CONSTRAINT `proizvod_ibfk_2` FOREIGN KEY (`proizvodjacID`) REFERENCES `proizvodjac` (`proizvodjacID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Metalni profili','Met1-AUT',1,2),(2,'Cevi','Met2-AUT',1,2),(3,'X-Ray','Zdr1-AUT',2,3),(4,'Mikroskop','Zdr2-AUT',2,3),(5,'Igla','Zdr3-AUT',3,3),(6,'Vakcina','Zdr4-AUT',3,3),(7,'Knjige','Obr1-AUT',4,4),(8,'Zivotno Osig','Osi1-FRA',5,5),(9,'Zdavstveno Osig','Osi2-FRA',5,5),(10,'Putno Osig','Osi3-FRA',5,5),(11,'Stambeni Kred','Fin1-FRA',6,1),(12,'Kes Kred','Fin2-FRA',6,1),(13,'Auto Kred','Fin3-FRA',6,1),(14,'Mleko','Agr1-FRA',7,6),(15,'Jogurt','Agr2-FRA',7,6),(16,'Sir','Agr3-FRA',7,6),(17,'Gas','Pet1-FRA',8,7),(18,'Djubrivo','Pet2-FRA',8,7),(19,'Benzin','Pet1-BRA',9,7),(20,'Kerozin','Pet2-BRA',9,7),(21,'Dizel','Pet3-BRA',9,7),(22,'Gas','Pet4-BRA',9,7),(23,'Benzin','Pet1-BRA',10,7),(24,'Avio Prevoz','Tra1-PAN',11,8),(25,'Automobil','Aut1-CZE',12,9),(26,'Kombi','Aut2-CZE',12,9),(27,'Mleko','Agr1-CZE',13,6),(28,'Jogurt','Agr2-CZE',13,6),(29,'Zimska Guma','Aut3-CZE',14,9),(30,'Letnja Guma','Aut4-CZE',14,9),(31,'Stambeni Kred','Fin1-SRB',16,1),(32,'Gaming','IT1-SRB',17,10),(33,'SW','IT2-SRB',19,10),(34,'Testiranje','IT3-SRB',21,10),(35,'HW','IT4-SRB',18,10);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `proizvodjacID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `gradID` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodjacID`),
  KEY `fk_gradID` (`gradID`),
  CONSTRAINT `proizvodjac_ibfk_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'BHP Group',11),(2,'CSL Limited',11),(3,'Barwon ',10),(4,'Deakin',10),(5,'AXA',9),(6,'BNP Paribas',9),(7,'Lactalis',8),(8,'Biogas Plant',8),(9,'Petrobras',7),(10,'Petrobras',6),(11,'COPA HOLDINGS',5),(12,'Skoda Auto',4),(13,'Agrofert',4),(14,'Gumotex',3),(15,'Helian Group',3),(16,'Delta',2),(17,'Nordeus',2),(18,'Comtrade',2),(19,'Levi9',1),(20,'Vega IT',1),(21,'Symphony',1);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-18 11:45:30
